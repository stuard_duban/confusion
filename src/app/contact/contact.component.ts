import { Component, OnInit, ViewChild, Inject } from '@angular/core';

//import { Feedback } from '../shared/feedback';


import { Location } from '@angular/common';
import { switchMap } from 'rxjs/operators';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Feedback, ContactType } from '../shared/feedback';
import { FeedbackService } from '../services/feedback.service';

import { flyInOut, visibility } from '../animations/app.animation';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block'
  },
  animations: [
    flyInOut(),
    visibility()
  ]
})
export class ContactComponent implements OnInit {

  @ViewChild('fform') feedbackFormDirective;

  feedbackForm: FormGroup;        //Variable to fetch information from form
  feedback: Feedback;
  contactType = ContactType;      //Const which contains way of contacting
  feedbackcopy: Feedback;

  flag: boolean;
  errMess: string;

  visibilitySubmtData = 'hidden';

  //feedbackcopy: Feedback;

  formErrors = {
    'firstname': '',
    'lastname': '',
    'telnum': '',
    'email': ''
  };

  validationMessages = {
    'firstname': {
      'required':      'First Name is required.',
      'minlength':     'First Name must be at least 2 characters long.',
      'maxlength':     'FirstName cannot be more than 25 characters long.'
    },
    'lastname': {
      'required':      'Last Name is required.',
      'minlength':     'Last Name must be at least 2 characters long.',
      'maxlength':     'Last Name cannot be more than 25 characters long.'
    },
    'telnum': {
      'required':      'Tel. number is required.',
      'pattern':       'Tel. number must contain only numbers.'
    },
    'email': {
      'required':      'Email is required.',
      'email':         'Email not in valid format.'
    },
  };

  constructor(private fb: FormBuilder,
  private feedbackservice: FeedbackService,
  @Inject('BaseURL') private BaseURL) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.feedbackForm = this.fb.group({
      firstname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)] ],
      lastname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)] ],
      telnum: ['', [Validators.required, Validators.pattern] ],
      email: ['', [Validators.required, Validators.email] ],
      agree: false,
      contacttype: 'None',
      message: ''
    });

    this.feedbackForm.valueChanges
    .subscribe(data => this.onValueChanged(data));
    this.onValueChanged();        //Reset form validation messages
  }

  onValueChanged(data?: any) {
    if(!this.feedbackForm) { return; }
    const form = this.feedbackForm;

    for(const field in this.formErrors) {
      //If alredy have an error clear all of them
      if(this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        //If there is no a valid or clear field show an error
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for(const key in control.errors) {
            //If there is error in specific key assign the message
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }

  /*When the form is submited*/
  onSubmit() {
    this.feedback = this.feedbackForm.value;


    //this.feedbackservice.putFeedback(this.feedback)
    this.feedbackcopy = this.feedbackForm.value;

  this.feedbackservice.submitFeedback(this.feedback)
      .subscribe( feedback => {
        this.feedback = feedback; this.feedbackcopy = feedback;
        this.visibilitySubmtData = 'shown'
      },
      errmess => { this.feedback = null; this.feedback = null; this.errMess = <any>errmess});

      setTimeout(() => { this.flag=true;}, 5000);
    //return of(LEADERS).pipe(delay(2000));
    console.log(this.feedbackcopy);
    this.feedbackForm.reset({
      firstname: '',
      lastname: '',
      telnum: '',
      email: '',
      agree: false,
      contacttype: 'None',
      message: ''
    });
    this.flag = false;
    this.feedbackcopy = null;
    
    this.feedbackFormDirective.resetForm();
  }

}
