import { Component, OnInit, Input, ViewChild, Inject } from '@angular/core';

import { Dish } from '../shared/dish';

import { DishService } from '../services/dish.service';

import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { switchMap } from 'rxjs/operators';
//import { Comment } from '../shared/comment';
//Importing modules for form
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Feedback, ContactType } from '../shared/feedback';

import { visibility, flyInOut, expand } from '../animations/app.animation';

import { trigger, state, style, animate, transition } from '@angular/animations';
@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  animations: [
    flyInOut(),
    visibility(),
    expand()
  ]
})

export class DishdetailComponent implements OnInit {

  dish: Dish;
  dishIds: string[];
  prev: number;
  next: number;
  errMess: string;

  defaultValueSlider: number;

  dishcopy: Dish;
  visibility = 'shown';

  date: Date;

  /*.controller('DishdetailComponent', ['$scope', function ($scope) {
    $scope.myDiscreteValue = 5; //Will set the slider to a value of 5
  }]);*/

  @ViewChild('fform') feedbackFormDirective;

  feedbackForm: FormGroup;        //Variable to fetch information from form
  feedback: Feedback;
  contactType = ContactType;      //Const which contains way of contacting

  //const myObjStr = JSON.stringify(feedbackForm);

  constructor(private dishservice: DishService,
    private route: ActivatedRoute,
    private location: Location,
    private fb: FormBuilder,
    @Inject('BaseURL') private BaseURL) {

      //this.myDiscreteValue();
    }
  ngOnInit() {
    this.createForm();
    this.dishservice.getDishIds().subscribe(dishIds => this.dishIds = dishIds);

    this.route.params
      .pipe(switchMap((params: Params) => {this.visibility = 'hidden'; return this.dishservice.getDish(+params['id'])} ))
      .subscribe(dish => { this.dish = dish; this.dishcopy = dish; this.setPrevNext(dish.id); this.visibility = 'shown'; },
      errmes => this.errMess = <any>errmes);

  }

  setPrevNext(dishId: number) {
    const index = this.dishIds.indexOf(String(dishId));
    this.prev =parseInt( this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length]);
    this.next =parseInt( this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length]);

  }

  goBack(): void {
    this.location.back();
  }
  //Array which contains errors
  formErrors = {
    'author': '',
    'comment': ''
  };
  //Array with validation messages
  validationMessages = {
    'author': {
      'required':      'Author Name is required.',
      'minlength':     'Author Name must be at least 2 characters long.',
      'maxlength':     'Author Name cannot be more than 25 characters long.'
    },
    'comment': {
      'required':      'Comment is required.',
    },
  };

  //Function to create the forms
  createForm() {
    this.date = new Date();





    this.feedbackForm = this.fb.group({
      author: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)] ],
      rating: '5',
      comment: ['', [Validators.required]],
      date: this.date.toISOString()
    });

    this.feedbackForm.valueChanges
    .subscribe(data => this.onValueChanged(data));
    //console.log(this.feedbackForm);
    this.onValueChanged();        //Reset form validation messages
  }

  onValueChanged(data?: any) {
    if(!this.feedbackForm) { return; }
    const form = this.feedbackForm;

    for(const field in this.formErrors) {
      //If alredy have an error clear all of them
      if(this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        //If there is no a valid or clear field show an error
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for(const key in control.errors) {
            //If there is error in specific key assign the message
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
    //console.log(JSON.stringify(this.feedbackForm));
  }

  /*When the form is submited*/
  onSubmit() {
    this.feedback = this.feedbackForm.value;
    //console.log(this.feedback);

    this.dishcopy.comments.push(this.feedbackForm.value);
    this.dishservice.putDish(this.dishcopy)
      .subscribe( dish => {
        this.dish = dish; this.dishcopy = dish;
      },
      errmess => { this.dish = null; this.dishcopy = null; this.errMess = <any>errmess});

    this.dishcopy
    this.feedbackForm.reset({
      author: '',
      rating: '5',
      comment: '',
      date: '',
    });
    this.feedbackFormDirective.resetForm();
  }
}
